export async function getData(url : string){
    const promise : Promise <Response> = fetch(url);
    try{
        const response : Response = await promise;
        const data = await response.json();
        return data;
    }
    catch(error){
        console.log(error);
    }
}