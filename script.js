import { getData } from "./fetch-api.js";
import { populateTable } from "./populate-table.js";
const url = "https://jsonplaceholder.typicode.com/users";
const data = await getData(url);
populateTable(data);
