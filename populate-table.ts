export function populateTable(data: Array<object>) :void {
    let tbody: HTMLTableElement | null = document.querySelector('#table tbody');
    data.forEach(function (elem:any) {
        
        let tr: HTMLTableRowElement | null = document.createElement('tr');
        tr.innerHTML= '<td>' + elem.id + '</td><td>' + elem.name + '</td><td>' + elem.username + '</td><td>'
            + elem.email + '</td><td>' + elem.address.city + '</td><td>' + elem.company.name + '</td>';
        tbody!.appendChild(tr);
    });
}
