export async function getData(url) {
    const promise = fetch(url);
    try {
        const response = await promise;
        const data = await response.json();
        return data;
    }
    catch (error) {
        console.log(error);
    }
}
